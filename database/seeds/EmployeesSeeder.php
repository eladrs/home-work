<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            [
                'employee' => 10,
                "manager" => 2
            ],
            [
                'employee' => 11,
                "manager" => 2
            ],
            [
                'employee' =>12,
                "manager" => 2
            ]
        ]);

    }
}
