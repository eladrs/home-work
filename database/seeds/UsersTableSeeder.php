<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'id'=>10,
                    'name'=>'moshe',
                    'email'=>'moshe@moshe.com',
                    'password'=>Hash::make('123'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'employee',
                ],
                [
                    'id'=>11,
                    'name'=>'gilad',
                    'email'=>'gilad@gilad.com',
                    'password'=>Hash::make('1234'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'employee',
                ],
                [
                    'id'=>12,
                    'name'=>'gad',
                    'email'=>'gad@gad.com',
                    'password'=>Hash::make('12345'),
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'employee',
                ],
            ]
            );
    }
}
