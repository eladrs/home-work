<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title' => 'Born to Run',
                'author' => 'Christopher McDougall ',
                'user_id' => '1',
                'created_at' => date('Y-m-d G:i:s'),
                'status' => 0
            ],
           /* [
                'title' => 'Predictably Irrational, Revised ',
                'author' => 'Dan Ariely  ',
                'user_id' => '2',
                'created_at' => date('Y-m-d G:i:s')
                'status' => 0
            ],
            [
                'title' => 'Rich Dad Poor Dad',
                'author' => 'Robert T. Kiyosaki ',
                'user_id' => '2',
                'created_at' => date('Y-m-d G:i:s')
                'status' => 0
            ],
            [
                'title' => 'Sapiens: A Brief History of Humankind',
                'author' => 'Yuval Noah Harari ',
                'user_id' => '1',
                'created_at' => date('Y-m-d G:i:s')
                'status' => 0
            ],
            [
                'title' => 'The Alchemist',
                'author' => 'Paulo Coelho ',
                'user_id' => '1',
                'created_at' => date('Y-m-d G:i:s')
                'status' => 0
            ]
*/
        ]);
    }
}
