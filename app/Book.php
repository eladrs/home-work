<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
     //database fields to be allwed for massive assigment
     protected $fillable =[
        'title',
        'author',
        'status',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}